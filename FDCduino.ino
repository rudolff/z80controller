/*
 * FDCdu.ino
 *
 * Created: 27.05.2018 18:06:44
 * Author : Виталий
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <stdio.h>
#include "print.h"
#include "PetitFS.h"
#include "wd1793.h"

#define GET_DATA() ((PINC & 0b00111111)|((PINB & 0b11)<<6))

#define GET_ADDR() ((PIND>>4) & 0b11)

#define INEN_PIN 2
#define GET_INEN()  (PIND & _BV(INEN_PIN)) 
#define A7_PIN 3
#define GET_ADDR_A7() (PIND & _BV(A7_PIN))
#define WR_PIN 7
#define GET_WR()   (PIND & _BV(WR_PIN))
#define RD_PIN 6
#define GET_RD()   (PIND & _BV(RD_PIN))


#define WAIT_PIN 0
#define START_WAIT()  (PORTD &= ~_BV(WAIT_PIN)) 
#define STOP_WAIT()   (PORTD |= _BV(WAIT_PIN))


unsigned char PortFF = 0;

inline void SET_DATA(uint8_t d) {
	 PORTC = d & 0b00111111; 
	 PORTB = (PORTB & 0b11111100) | ((d >> 6) & 0b11);
}

// Faster function sets bits 7,6 only
inline void SET_DATA_PORTFF(uint8_t d){
	PORTB = (PORTB & 0b11111100) | ((d >> 6) & 0b11);
}

inline void SET_DATA_OUT(){
	DDRC = 0b00111111;
	DDRB |= 0b11;
}

inline void SET_DATA_OUT_PORTFF(){
	DDRB |= 0b11;
}

inline void SET_DATA_IN(){
	DDRC = 0;
	DDRB &= ~0b11;
}

void initPorts()
{
	DDRD = _BV(WAIT_PIN);

	PORTB = 0xff; // Input with pull ups	
	PORTC = 0xff; // Input with pull ups
}

void initTimer()
{
	TCCR1B = _BV(CS11); // clk/8
	TIMSK1 = _BV(TOIE1); // Overflow interrupt
}

ISR(INT0_vect) // On low level of INEN of WD1793
{
	unsigned char data;

	if(!GET_RD()) {	
			
		if(GET_ADDR_A7()) { // PortFF
			data = WD1793_GetRequests();
			SET_DATA_PORTFF(data);
			SET_DATA_OUT_PORTFF();
			//printf("RD 0x%02X %c from %i\n", data, data, GET_ADDR());
		}
		else {
			data = WD1793_Read(GET_ADDR());
			SET_DATA(data);
			SET_DATA_OUT();
			//printf("RD 0x%02X %c from %i\n", data, data, GET_ADDR());
		}
		STOP_WAIT();
		while(!GET_INEN()); // wait release of /INEN
		SET_DATA_IN();
	}
	else if(!GET_WR()) {
		if(GET_ADDR_A7()) { // PortFF
			PortFF = GET_DATA();
			//printf("WR 0x%02X %c to %i\n", PortFF, PortFF, GET_ADDR());
		}
		else {
			//printf("WR 0x%02X to %i\n", GET_DATA(), GET_ADDR());
			WD1793_Write(GET_ADDR(), GET_DATA());
		}
		STOP_WAIT();
		while(!GET_INEN()); // wait release of /INEN
	}
	else // There are select signals without RD/WR operation specified
	{
		STOP_WAIT();
		while(!GET_INEN()); // wait release of /INEN
	}
	
	START_WAIT();

}

void setup() 
{
  initPorts();
  //print_init();
  initTimer();
  START_WAIT(); 
  EIMSK = 1; // enable INT0
  sei();
}

void loop()
{
	WD1793_Execute();
}




//------------------------------------------------------------------------------
